require 'twitter-text'

require 'models/project'
require 'models/span'

module Controllers
	module Composer
		private
		def self.contains_entities?(tweet)
			return true if Twitter::Extractor.
				extract_hashtags(tweet).
				length > 0
			return true if Twitter::Extractor.
				extract_mentioned_screen_names(tweet).
				length > 0
			return true if Twitter::Extractor.
				extract_urls(tweet).
				length > 0

			return false
		end

		def self.start_project(user, text)
			fail if !user
			fail if (text.strip == "")

			project = Models::Project.new do |p|
				p.finished = false
			end

			DB.transaction do
				project.save

				amend_project(user, project, text)
			end
		end

		def self.amend_project(user, project, new_text)
			fail if !user
			fail if !project
			fail if (text.strip == "")

			# check to see if the amended text contains any entities
			# and reject if so
			amended_text = project.full_text + new_text
			fail if contains_entities?(amended_text)

			DB.transaction do
				span = Models::Span.new do |s|
					s.author = user
					s.project = project
					s.text = new_text
				end

				span.save
			end
		end
	end
end