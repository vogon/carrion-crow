require 'models/twitter_user'

module Controllers
	module Twitter
		def self.update_twitter_user(uid, options = {})
			user = get_user(uid)

			if !user
				user = Models::TwitterUser.new do |u|
					u.uid = uid
				end
			end

			DB.transaction do
				if options.has_key? :handle
					user.handle = options[:handle]
				end

				if options.has_key? :picture_uri
					user.picture_uri = options[:picture_uri]
				end

				user.save
			end

			return user
		end

		def self.get_user(uid)
			Models::TwitterUser[uid]
		end
	end
end