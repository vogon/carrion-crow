require 'omniauth-twitter'
require 'sass'
require 'sinatra'
require 'slim'

require 'controllers/twitter'

class CarrionCrow < Sinatra::Base
	enable :sessions

	before do
		@user = Controllers::Twitter.get_user(session[:user_id])
	end

	helpers do
		def logged_in?
			@user != nil
		end

		def session_username
			nil if !logged_in?
			@user.handle
		end

		def session_picture_uri
			nil if !logged_in?
			@user.picture_uri
		end
	end

	get '/' do
		slim :main
	end

	get '/compose' do
		slim :compose
	end

	get '/login' do
		redirect '/auth/twitter'
		200
	end

	get '/logout' do
		session[:user_id] = nil

		redirect '/'
		200
	end

	use OmniAuth::Builder do
		provider :twitter, USER_CONSUMER_KEY, USER_CONSUMER_SECRET
	end

	get '/auth/twitter/callback' do
		twinfo = env['omniauth.auth']

		Controllers::Twitter.update_twitter_user(twinfo[:uid],
			:handle => twinfo[:info][:nickname],
			:picture_uri => twinfo[:info][:image])

		session[:user_id] = twinfo[:uid]

		redirect "/"
		200
	end
end