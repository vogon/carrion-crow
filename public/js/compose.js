function stretchBodyToFill() {
	var windowHeight = window.innerHeight;
	var headerHeight = $('#header')[0].offsetHeight;

	$('#body').height(windowHeight - headerHeight - 40);
}

function recenterCenterBlock() {
	var centerBlockHeight = $('#body-center-block')[0].offsetHeight;
	var halfHeight = centerBlockHeight / 2;

	$('#body-floater').css('margin-bottom', -halfHeight);
}

$().ready(stretchBodyToFill);
$().ready(recenterCenterBlock);

$(window).resize(stretchBodyToFill);
$(window).resize(recenterCenterBlock);