require 'sequel'
require 'twitter-text'

require 'models/span'

module Models
	class Project < Sequel::Model
		one_to_many :spans, :class => "Models::Span"

		def full_text
			spans.map { |span| span.text }.join
		end

		def space_left
			140 - full_text.tweet_length
		end
	end
end