require 'sequel'

require 'models/twitter_user'

module Models
	class Span < Sequel::Model
		many_to_one :project, :class => "Models::Project"
		many_to_one :author, :class => "Models::TwitterUser"
	end
end