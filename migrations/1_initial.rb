require 'sequel'

Sequel.migration do
	change do
		create_table :twitter_users do
			# turn off autoincrementing because these UIDs live
			# somewhere else; use 64 bits just to be safe
			Bignum :uid, :primary_key => true

			String :handle
			String :picture_uri
		end

		create_table :projects do
			primary_key :project_id

			Boolean :finished
		end

		create_table :spans do
			primary_key :span_id

			foreign_key :project_id, :projects
			foreign_key :author_id, :twitter_users
			
			String :text
		end
	end
end