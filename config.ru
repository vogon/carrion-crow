$LOAD_PATH << "."

require 'sequel'

if ENV['RACK_ENV'] == 'development'
	puts "connecting to sqlite"
	DB = Sequel::connect('sqlite://test.db')
end

require 'sass/plugin/rack'

use Rack::Static, :urls => ['/img', '/js'], :root => 'public'

Sass::Plugin.options[:style] = :compressed
use Sass::Plugin::Rack

USER_CONSUMER_KEY = ENV['USER_CONSUMER_KEY']
USER_CONSUMER_SECRET = ENV['USER_CONSUMER_SECRET']

require 'carrioncrow'
run CarrionCrow